CXX			:= g++
CXX_FLAGS		:= -std=c++11 -Wall -Werror

BUILD_DIR		:= build/obj
TARGET_DIR		:= apps
INCLUDES		:= include
SOURCES			:= $(shell find source -iname *.cpp)
OBJECTS			:= $(addprefix $(BUILD_DIR)/,$(notdir $(SOURCES:.cpp=.o)))
TARGET			:= snake-term

.PHONY: all clean prepare  $(SOURCES)

all: prepare $(SOURCES) $(TARGET)

$(TARGET):
	$(CXX) -o $(TARGET_DIR)/$@ $(OBJECTS)

$(SOURCES):
	$(CXX) -c $(CXX_FLAGS) $@ -o $(BUILD_DIR)/$(notdir $(subst .cpp,.o,$@)) -I$(INCLUDES)

prepare:
	-@mkdir -p $(BUILD_DIR)
	-@mkdir -p $(TARGET_DIR)

clean:
	@rm -rf $(BUILD_DIR)
	@rm -rf $(TARGET_DIR)

