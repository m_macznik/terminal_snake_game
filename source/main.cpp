#include <iostream>
#include <string>
#include <unistd.h> // for sleep

#include <grid/grid.h>

int main(int         argc
        ,char        **argv
        )
{
    Snake snake(std::make_pair(10, 10));
    Grid gameGrid(10, 10, &snake);

    while(true)
    {
        usleep(500000);
        if (gameGrid.handleSnake(&snake))
        {
            break;
        }
        gameGrid.printGrid();
    }
    std::cout << "GAME OVER" << std::endl;

    return 0;
}
