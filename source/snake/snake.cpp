#include <snake/snake.h>

Snake::Snake(std::pair<int, int> bounds)
{
    int max_x = bounds.first;
    int max_y = bounds.second;

    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> distributionX(0, max_x);
    std::uniform_int_distribution<> distributionY(0, max_y);
    int x = 0; //distributionX(generator);
    int y = 0; //distributionY(generator);
    this->dir = Direction::RIGHT;
    this->lastPos = std::make_pair(0,0); // default
    this->headPos = std::make_pair(x, y);
}

Snake::~Snake()
{
}

void Snake::setHeadPos(int x, int y)
{
    this->lastPos = this->headPos;
    this->headPos = std::make_pair(x, y);
}

const std::pair<int,int>& Snake::getHeadPos()
{
    return this->headPos;
}

const std::pair<int,int>& Snake::getLastHeadPos()
{
    return this->lastPos;
}

const std::list<std::pair<int,int>>& Snake::getTail()
{
    return this->tail;
}

int Snake::getSnakeLength()
{
    return this->tail.size();
}

Direction& Snake::getCurrentDirection()
{
    return this->dir;
}

std::pair<int, int> Snake::move()
{
    const std::pair<int,int>& currHead = this->getHeadPos();
    switch (this->dir)
    {
        case Direction::UP:
            this->setHeadPos(currHead.first, currHead.second - 1);
            break;
        case Direction::DOWN:
            this->setHeadPos(currHead.first, currHead.second + 1);
            break;
        case Direction::LEFT:
            this->setHeadPos(currHead.first - 1, currHead.second);
            break;
        case Direction::RIGHT:
            this->setHeadPos(currHead.first + 1, currHead.second);
            break;
        default:
            break;
    }
    if (0 < this->getSnakeLength()) // if snake has tail
    {
        std::pair<int,int> last = this->tail.back();
        this->tail.pop_back(); // remove last element from tail
        this->tail.push_front(this->lastPos);
        return last;
    }
    else
        return std::make_pair(0,0);
}

void Snake::eat()
{
    this->tail.push_back(this->getLastHeadPos());
}
