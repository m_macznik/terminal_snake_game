#include "grid/grid.h"

Field::Field()
{
    this->fieldID = '.';
}

Field::~Field()
{
}

void Field::setFieldType(char type)
{
    this->fieldID = type;
}

const char& Field::getFieldType()
{
    return this->fieldID;
}

Grid::Grid(int rows, int cols, Snake *snake)
{
    this->grid.clear();
    this->isFruitPresent = false;
    this->size = std::make_pair(rows, cols);
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            this->grid.push_back(new Field());
        }
    }
    this->setXYFieldType(snake->getHeadPos(), 'O');
}

Grid::~Grid()
{
    Field *p_field;
    for (int i = 0; i < this->size.first; ++i)
    {
        for (int j = 0; j < this->size.second; ++j)
        {
            p_field = this->grid[i * this->size.first + j];
            delete p_field;
        }
    }
}

void Grid::putFruit()
{
    while (!this->isFruitPresent)
    {
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<> 
            distX(0, this->size.first);
        std::uniform_int_distribution<> 
            distY(0, this->size.second);
        int x = distX(generator);
        int y = distY(generator);
        if ('.' == this->getXYFieldType(std::make_pair(x,y)))
        {
            this->setXYFieldType(std::make_pair(x,y), 'X');
            this->isFruitPresent = true;
        }
    }
}

const char& Grid::getXYFieldType(std::pair<int,int> pos)
{
    int x = pos.first;
    int y = pos.second;
    Field *p_field = this->grid[ y * this->size.first + x ];
    return p_field->getFieldType();
}

void Grid::setXYFieldType(std::pair<int,int> pos, char type)
{
    int x = pos.first;
    int y = pos.second;
    Field *p_field = this->grid[y * this->size.first + x];
    p_field->setFieldType(type);
}

bool Grid::snakeHasBoundsCollision(Snake *snake)
{
    if (
        (  (snake->getHeadPos().first >= this->size.first)
        && (snake->getCurrentDirection() == Direction::RIGHT)
        )
        ||
        (  (snake->getHeadPos().first < 0)
        && (snake->getCurrentDirection() == Direction::LEFT)
        )
        ||
        (  (snake->getHeadPos().second >= this->size.second)
        && (snake->getCurrentDirection() == Direction::DOWN)
        )
        ||
        (  (snake->getHeadPos().second < 0)
        && (snake->getCurrentDirection() == Direction::UP)
        )
       )
    {
        return true;
    }
    return false;
}

bool Grid::snakeHasSelfCollision(Snake *snake)
{
    const char& field = this->getXYFieldType(snake->getHeadPos());
    return 'o' == field ? true : false;
}

bool Grid::snakeHasCollision(Snake *snake)
{
    if (this->snakeHasBoundsCollision(snake))
        return true;
    
    if (this->snakeHasSelfCollision(snake))
        return true;

    return false;
}

bool Grid::handleSnake(Snake *snake)
{
    this->putFruit();
    std::pair<int,int> last_element = snake->move();
    if (this->snakeHasCollision(snake))
    {
        return true;
    }

    const char& fieldType = this->getXYFieldType(snake->getHeadPos());
    if ('X' == fieldType)
    {
        snake->eat();
        this->isFruitPresent = false;
    }

    // handle head
    this->setXYFieldType(snake->getLastHeadPos(), '.');
    this->setXYFieldType(snake->getHeadPos(), 'O');
    
    if (0 < snake->getSnakeLength())
    {
        const std::list<std::pair<int,int>>& tail = snake->getTail();
        for (std::pair<int,int> tailPart : tail)
        {
            this->setXYFieldType(tailPart, 'o');
        }
        this->setXYFieldType(last_element, '.');
    }

    return false;
}

void Grid::printGrid()
{
    system("clear");
    for (int i = 0; i < this->size.first; ++i)
    {
        for (int j = 0; j < this->size.second; ++j)
        {
            const char& fieldType = this->getXYFieldType(std::make_pair(j, i));
            std::cout << fieldType << " ";
        }
        std::cout << std::endl;
    }
}
