#ifndef __GRID_H
#define __GRID_H

#include <iostream>
#include <cstdlib>
#include <utility>
#include <vector>

#include "snake/snake.h"

class Field
{
    private:
        char fieldID;
    public:
        Field();
        ~Field();
        void setFieldType(char type);
        const char& getFieldType();
};

class Grid
{
    private:
        std::pair<int, int> size;
        std::vector<Field*> grid;
        bool isFruitPresent;
        bool snakeHasSelfCollision(Snake *snake);
        bool snakeHasBoundsCollision(Snake *snake);
        bool snakeHasCollision(Snake *snake);
    public:
        Grid(int rows, int cols, Snake *snake);
        ~Grid();
        void putFruit();
        const char& getXYFieldType(std::pair<int,int> pos);
        void setXYFieldType(std::pair<int,int> pos, char type);
        bool handleSnake(Snake *snake);
        void printGrid();
};

#endif /* __GRID_H */
