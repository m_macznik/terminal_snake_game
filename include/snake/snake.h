#ifndef __SNAKE_H
#define __SNAKE_H
#include <utility>
#include <random>
#include <list>

enum class Direction { UP, DOWN, LEFT, RIGHT };

class Snake
{
    private:
        std::pair<int, int> headPos;
        std::pair<int, int> lastPos;
        std::list<std::pair<int,int>> tail;
        Direction dir;
    public:
        Snake(std::pair<int, int> bounds);
        ~Snake();
        Direction& getCurrentDirection();
        void setHeadPos(int x, int y);
        const std::pair<int,int>& getHeadPos();
        const std::pair<int,int>& getLastHeadPos();
        const std::list<std::pair<int,int>>& getTail();
        int getSnakeLength();
        std::pair<int,int> move();
        void eat();
};

#endif
